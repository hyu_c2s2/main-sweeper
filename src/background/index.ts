chrome.contextMenus.create({
  id: "Extract",
  title: "Run GCE Algorithm",
  onclick: (info, tab) => {
    const tabId = tab.id
    if (!!tabId) {
      chrome.tabs.sendMessage(tabId, {
        request: 'extract',
        method: 'gce'
      }, response => {
        console.log(JSON.parse(response))
      })
    }
  }
})