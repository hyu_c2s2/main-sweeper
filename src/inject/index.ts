import { runOnContentScript } from "../extraction"

export function main() {
  console.log("Intialize Content Script")
  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    const { request, method } = message
    if (request === 'extract' && method === 'gce') {
      const result = runOnContentScript()
      if (result !== null) {
        const candidate = {
          ...result,
          el: result.el.outerHTML,
          seed: result.seed.outerHTML
        }
        sendResponse(JSON.stringify(candidate))
      }
      else {
        sendResponse("No Result")
      }
      return true
    }
  })
  console.log("Ready")
}

main()