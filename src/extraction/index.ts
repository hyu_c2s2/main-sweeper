import { extract } from "./gce"
import { IsNotReadableException } from "./gce/@types"

export function runOnContentScript() {
  try {
    const result = extract(document)
    return result
  }
  catch (err) {
    console.error("Error on GCE Extraction")
    if (err instanceof IsNotReadableException) {
      return null
    }
    else {
      throw new Error(`${err}`)
    }
  }
}