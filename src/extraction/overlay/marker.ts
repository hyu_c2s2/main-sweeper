import { Border } from "./border"
import $ from 'jquery'

export function createMarker(coordinates: [number, number], color = "red", text = "marker"): HTMLElement {
  const marker = document.createElement("div")
  const textNode = document.createTextNode(text)
  marker.appendChild(textNode)
  marker.style.position = "absolute"
  $(marker).offset({ left: coordinates[0], top: coordinates[1] })
  $(marker).addClass("hyu marker")
  document.body.appendChild(marker)

  const border = new Border({ color: color, thickness: "20px" })
  border.cover(marker)
  border.text = text
  return marker
}