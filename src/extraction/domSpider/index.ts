import WebPage from "./webpage"


export function inspectDocument(document: Document) {
  const webpage = new WebPage(document)
  webpage.indexing()
  return webpage.nodes
}