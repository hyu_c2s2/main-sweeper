export function nodesUnder(el?: Node, showTexts?: boolean, filter?: NodeFilter | null) {
  const nodes = []
  const root = el ? el : document.body
  const walker = document.createNodeIterator(root, showTexts ? NodeFilter.SHOW_TEXT : NodeFilter.SHOW_ELEMENT, filter);
  let node
  while (node = walker.nextNode()) {
    nodes.push(node);
  }
  return nodes;
}


export function wrappingTextNodes(textNodes: Node[]) {
  textNodes.forEach(textNode => {
    const txt = textNode as CharacterData
    if (txt.data.trim() === '') return

    const newSpan = document.createElement('span')
    newSpan.appendChild(txt.cloneNode())
    newSpan.className = 'hyu wrapped'
    txt.after(newSpan)
    txt.remove()
  })
}
