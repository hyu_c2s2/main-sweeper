import $ from 'jquery'
import { Border } from '../overlay'

type Coordinates = [number, number]


export function getElementSize(el: Element) {
  const { width, height } = el.getBoundingClientRect()
  return width * height
}

export function getScreenCenter(doc = document): Coordinates {
  const window = doc.defaultView!

  const left = window.innerWidth / 2
  const top = window.innerHeight / 2
  return [left, top]
}

export function getDocumentCenter(doc = document): Coordinates {
  const { width, height } = getDocumentSize(doc)

  const left = Math.min(window.innerWidth, width) / 2
  const top = height / 2
  return [left, top]
}

export function getDocumentSize(doc = document) {
  const html = doc.documentElement
  const body = doc.body
  const height = Math.max(body.scrollHeight, body.offsetHeight,
    html.clientHeight, html.scrollHeight, html.offsetHeight);
  const width = Math.max(body.scrollWidth, body.offsetWidth,
    html.clientWidth, html.scrollWidth, html.offsetWidth);

  return { height: height, width: width }
}

export function* iterGrids(doc = document, vertical = 4, horizontal = 6) {
  const window = doc.defaultView!

  const windowHeight = window.innerHeight
  const windowWidth = window.innerWidth

  const width = windowWidth / horizontal
  const height = windowHeight / vertical
  const contentHeight = getDocumentSize(doc).height
  console.log("GRID SIZE", vertical, horizontal)
  console.log("CELL SIZE", width, height)
  const heightGrid = Math.ceil(Math.min(contentHeight, windowHeight * 2) / height) - 1
  for (const h of [...Array(heightGrid).keys()]) {
    for (const w of [...Array(horizontal).keys()]) {
      const existGrid = doc.querySelector(`.hyu.grid.w-${w}.h-${h}`)
      if (existGrid) yield { w: w, h: h, el: existGrid as HTMLDivElement, width: width, height: height, widthGrid: horizontal, heigthGrid: heightGrid }
      else {
        const gridBox = doc.createElement('div')
        gridBox.textContent = `${w}, ${h}`
        gridBox.style.position = "absolute"
        $(gridBox).offset({ left: w * width, top: h * height })
        $(gridBox).addClass('hyu')
        $(gridBox).addClass('grid')
        $(gridBox).addClass(`w-${w}`)
        $(gridBox).addClass(`h-${h}`)
        $(gridBox).width(`${width}px`)
        $(gridBox).height(`${height}px`)
        doc.body.appendChild(gridBox)
        yield { w: w, h: h, el: gridBox as HTMLDivElement, width: width, height: height, widthGrid: horizontal, heigthGrid: heightGrid }
      }
    }
  }
}

export function toggleGrids(doc: Document = document) {
  const body = doc.body
  $('.hyu.grid', body).toggle()
  $('.hyu.grid', body).each((index, el) => {
    Border.toggle(el)
  })
}
