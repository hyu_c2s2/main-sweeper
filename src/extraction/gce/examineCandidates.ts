import { getElementSize } from "../domMeasures/centers"
import { Centroid, IsNotReadableException, MainContentCandidates, MainContentProperties, MainContentResult } from "./@types"

export function examineCandidates(candidates: MainContentCandidates[], centroids: Centroid[]): MainContentResult {
  const filtered = candidates.filter(({ el }) => el !== document.body)

  if (filtered.length === 0) {
    console.warn("Not readable pages: All Candidates are <body>")
    throw new IsNotReadableException()
  }

  const allSpan = document.body.querySelectorAll('span.hyu.wrapped')
  const allTextCount = Array.from(allSpan).reduce<number>((acc, cur) => cur.textContent ? acc + cur.textContent.trim().length : 0, 0)
  const calced = filtered.map(candi => {
    const { el, seed } = candi
    const spans = el.querySelectorAll('span.hyu.wrapped')
    const textCount = Array.from(spans).reduce<number>((acc, cur) => cur.textContent ? acc + cur.textContent.trim().length : 0, 0)
    const data: MainContentProperties = {
      spans: spans.length,
      spanRatio: spans.length / allSpan.length,
      spanDensity: spans.length / getElementSize(el),
      texts: textCount,
      textRatio: textCount / allTextCount,
      textDensity: textCount / getElementSize(el),
      clientRect: el.getBoundingClientRect(),
      seedRect: seed.getBoundingClientRect(),
      hyuIndex: el.getAttribute('hyu') ?? 'null'
    }
    return {
      ...candi,
      data: data
    }
  })

  const bests = calced.filter(o => o.best)
  return bests.length > 0 ? bests[0] : calced[0]
}