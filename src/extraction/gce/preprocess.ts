import { nodesUnder, wrappingTextNodes } from "../domMeasures/texts"
import { inspectDocument } from "../domSpider"

export function preprocess(document: Document) {
  inspectDocument(document)

  document.querySelectorAll(":not(.hyu)").forEach(el => {
    if (window.getComputedStyle(el).position === 'fixed' ||
      window.getComputedStyle(el).position === 'absolute') {
      el.setAttribute('hyu-fixed', 'true')
      el.querySelectorAll('*').forEach(d => d.setAttribute('hyu-fixed', 'true'))
    }
  })

  const rawTextNodes = nodesUnder(document.body, true)
  wrappingTextNodes(rawTextNodes)
}

