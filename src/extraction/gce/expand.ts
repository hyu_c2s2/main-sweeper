import { flatten, maxBy } from "lodash"
import { ARTICLE_BLOCK_ELEMENTS } from "../domMeasures"
import { getTextDensity } from "../domMeasures/areaAndRatio"
import { elementClosestDistance } from "../domMeasures/relation"
import { Centroid, MainContentCandidates, MainContentResult } from "./@types"
import { examineCandidates } from "./examineCandidates"



export function expand(centroids: Centroid[], textNodes: Element[]): MainContentResult {
  const cores = centroids.map(c => {
    const minDistElem = examineTextNodes(c, textNodes)
    return {
      name: c.name,
      ...minDistElem
    }
  })
  console.info("Main Content Cores", ...cores.map(({ dist, el }) => {
    return [dist, el]
  }))
  const extracted = flatten(cores.map(({ name, el }) => expandToMaincontent(
    el as HTMLElement, name)
  ))

  const result = examineCandidates(extracted, centroids)
  return result
}

function examineTextNodes(centroid: Centroid, textNodes: Node[]) {
  const minDistEl = {
    dist: Infinity,
    el: textNodes[0]
  }

  textNodes.some(el => {
    try {
      const dist = elementClosestDistance(centroid.position, el as HTMLElement)
      if (dist < minDistEl.dist) {
        minDistEl.dist = dist
        minDistEl.el = el
      }
    }
    catch (err) {
      console.error(el)
    }
  })

  return minDistEl
}

function expandToMaincontent(seed: HTMLElement, centerName?: string) {
  const candidates: MainContentCandidates[] = []
  candidates.push({
    name: 'ArticleTag',
    desc: 'Article Tag',
    el: expandUntil(seed, (el) => {
      return el.tagName === 'ARTICLE'
    }),
    seed: seed
  })

  const contentLikeClassNames = ['content', 'article']

  candidates.push({
    name: 'AttrSemantic',
    desc: 'Tag attributes has content like name',
    el: expandUntil(seed, (el) => {
      const checkList = Array.from(el.attributes).filter(e => !e.name.startsWith('hyu')).map(attr => attr.value)
      const result = checkList.map(token =>
        contentLikeClassNames.map(name => {
          return token.toLowerCase().includes(name)
        }).some(e => e)
      )
      return result.some(e => e)
    }),
    seed: seed
  })

  const maxElem = maxBy(candidates, ({ el }) => {
    if (el === document.body) return -1
    if (el.getBoundingClientRect().height < window.innerHeight / 2) return -1
    console.info(`BEST Candidate on ${centerName}`, el, el.id, getTextDensity(el))
    return getTextDensity(el)
  })
  if (maxElem) maxElem.best = true
  return candidates.map(candidate => ({
    centerName: centerName || "Unknown Centroid",
    ...candidate
  }))
}


function expandUntil(el: HTMLElement, condition: (cur: HTMLElement, parent: HTMLElement, ...args: any[]) => boolean) {
  let parent: HTMLElement | null = el
  let current: HTMLElement = el
  while (parent = current.parentElement) {
    if (ARTICLE_BLOCK_ELEMENTS.includes(current.tagName.toLowerCase()) && condition(current, parent)) break;
    current = parent
    if (current === document.body) break;
    if (!current.parentElement) break;
  }
  return current
}