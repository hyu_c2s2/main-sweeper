import { toggleGrids } from "../domMeasures/centers"

export function postprocess(document: Document) {
  toggleGrids(document)
  removeHyuElements(document)
}

function removeHyuElements(document: Document) {
  const targets: Element[] = []
  targets.push(...Array.from(document.querySelectorAll(".hyu.marker")))
  targets.push(...Array.from(document.querySelectorAll(".hyu.grid")))
  targets.forEach(el => el.remove())
}