import { filter } from "lodash"
import { countAnchorAreaRatio } from "../domMeasures/areaAndRatio"
import { iterGrids } from "../domMeasures/centers"
import { elementContains, elementOverlaps } from "../domMeasures/relation"
import { Border } from "../overlay"
import $ from 'jquery'

export function grid(document: Document) {
  drawGrids(document)
  countLinks(document)
  return Array.from(document.querySelectorAll('span.hyu.wrapped:not([hyunav]):not([hyu-fixed])'))
}

function drawGrids(document: Document, n = 4, m = 6) {
  const CONTENT_WIDTH = 1280
  const gridBoxs: HTMLDivElement[] = []
  const grids = iterGrids(document, n, m)

  for (const { w, h, el } of grids) {
    let options = { color: 'black' }
    $(el).attr("deadzone", 'true')
    if (h > 0 && (w > 0 && w < 5)) {
      options.color = 'green'
      $(el).attr("deadzone", 'false')
    }
    const border = new Border(options)
    border.cover(el)
    gridBoxs.push(el)
  }

  return gridBoxs
}

function countLinks(document: Document) {
  const LINK_AREA_RATIO_HEAVY = 0.75
  const LINK_AREA_RATIO_LIGHT = 0.5
  const anchorAreaRatioList = countAnchorAreaRatio(document)

  anchorAreaRatioList.forEach(({ ratio, el }, index, arr) => {
    el.setAttribute('hyu-link-area-ratio-rank', `${index}/${arr.length}`)
  })

  document.querySelectorAll('span.hyu.wrapped').forEach(node => {
    const parent = node.closest('[hyu-fixed]')
    if (parent) {
      node.setAttribute('hyu-fixed', 'true')
    }
  })

  document.querySelectorAll('span.hyu.wrapped').forEach(node => {
    const parent = node.closest('[hyu-link-area-ratio]')
    if (parent) {
      const ratio = parent.getAttribute('hyu-link-area-ratio')
      if (ratio && parseFloat(ratio) > LINK_AREA_RATIO_HEAVY) node.setAttribute('hyunav', 'heavy')
      if (ratio && parseFloat(ratio) > LINK_AREA_RATIO_LIGHT) node.setAttribute('hyunav', 'light')
    }
  })

  const navCandidatesHeavy = filter(anchorAreaRatioList, o => o.ratio > LINK_AREA_RATIO_HEAVY)
  const navCandidatesLight = filter(anchorAreaRatioList, o => o.ratio > LINK_AREA_RATIO_LIGHT && o.ratio <= LINK_AREA_RATIO_HEAVY)
  const grids = iterGrids()
  for (const { w, h, el: grid } of grids) {
    navCandidatesLight.some(({ ratio, el }) => {
      const overlap = elementOverlaps(grid, el) || elementContains(grid, el)
      if (overlap) {
        grid.style.backgroundColor = `rgba(160, 160, 0, 0.8)`
        $(grid).attr('deadzone', 'true')
        return true
      }
    })
    navCandidatesHeavy.some(({ ratio, el }) => {
      const overlap = elementOverlaps(grid, el) || elementContains(grid, el)
      if (overlap) {
        const borderId = parseInt($(grid).attr('border')!)
        const borders = filter(Border.borders, { 'id': borderId })
        borders?.forEach(border => {
          border.color = 'red'
        })
        grid.style.backgroundColor = `rgba(255, 0, 0, 0.5)`
        $(grid).attr('deadzone', 'true')

        return true
      }
    })
    if (w >= 5 && h >= 6) break;
  }
}