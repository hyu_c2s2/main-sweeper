import { mean } from "lodash"
import { getDocumentCenter, getScreenCenter, iterGrids } from "../domMeasures/centers"
import { elementCenter } from "../domMeasures/relation"
import { createMarker } from "../overlay"
import $ from 'jquery'
import { Centroid } from "./@types"


export function center(document: Document) {
  const grids = iterGrids()
  const lefts: number[] = []
  const tops: number[] = []
  let curWidthGrid: number = 0
  for (const { w, h, el, widthGrid } of grids) {
    curWidthGrid = widthGrid
    if ($(el).attr('deadzone') === 'false') {
      const { left, top } = elementCenter(el)
      lefts.push(left)
      tops.push(top)
    }
  }
  const centroids: Centroid[] = []

  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C1')
  centroids.push({ name: 'centroid', position: { left: mean(lefts), top: mean(tops) } })

  const [screenLeft, screenTop] = getScreenCenter(document)
  lefts.push(screenLeft)
  tops.push(screenTop)
  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C2')
  centroids.push({ name: 'centerCentroid', position: { left: mean(lefts), top: mean(tops) } })

  const docCoef = Math.max(curWidthGrid - 2, 1) // doc 중심에 green  grid 있다고 가정
  const [docLeft, docTop] = getDocumentCenter()
  lefts.push(docLeft)
  tops.push(docTop * docCoef)
  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C3')
  centroids.push({ name: 'centerDocCentroid', position: { left: mean(lefts), top: mean(tops) } })

  return centroids
}