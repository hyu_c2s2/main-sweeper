import { ElementPosition } from "../../domMeasures";

export interface GceOptions {
  cleanUp?: number
}


export interface Centroid {
  name: string
  position: ElementPosition
}

export interface MainContentCandidates {
  name: string
  desc?: string
  el: HTMLElement
  best?: boolean
  seed: HTMLElement
  centerName?: string
}

export interface MainContentProperties {
  spans: number
  spanRatio: number
  spanDensity: number
  texts: number
  textRatio: number
  textDensity: number
  clientRect: DOMRect
  seedRect: DOMRect
  hyuIndex: string
}

export type MainContentResult = MainContentCandidates & {
  data: MainContentProperties
}

export class IsNotReadableException extends Error {
  constructor(message?: string) {
    super(`All candidates are <body>: ${message}`)
    this.name = 'isNotReadableException'
  }
}