import { preprocess } from "./preprocess"
import { center } from "./center"
import { expand } from "./expand"
import { grid } from "./grid"
import { postprocess } from "./postprocess"
import { Border } from "../overlay"
import { GceOptions } from "./@types"


export function extract(document: Document, options?: GceOptions) {
  const cleanUp = options?.cleanUp || 1
  console.log("Options", cleanUp)
  const window = document.defaultView
  if (window === null) throw new Error("Missin Window object from document")

  preprocess(document)
  const textNodes = grid(document)
  const centroids = center(document)
  const result = expand(centroids, textNodes)

  const border = new Border({ color: '#CC5500', thickness: '8px' })
  border.text = `${result.name} - ${result.centerName} - BEST`
  border.cover(result.el)

  if (cleanUp > 0) {
    postprocess(document)
  }
  return result
}
