# main-sweeper
 A heuristic method for webpage main content extraction by imitating human reading perception.


# Related Repos
[Google TabNet Models](https://github.com/dreamwayjgs/main-sweeper-tabnet)


# Status
 Developing version in gitlab https://gitlab.com/dreamwayjgs/main-content-extractor-v2 (private repo now)


# Dataset
## Postgres

[Download here](https://dbnas.hanyang.ac.kr/s/KH7t9w8sY6RxxBQ/download)

Dumping code:

    pg_dump --quote-all-identifiers -Fc maincontent > maincontent_pg.dump

Restore example:

    pg_restore -d maincontent maincontent_pg.dump


## Mongodb

[Download here](https://dbnas.hanyang.ac.kr/s/9rcMqA9pwG5pFzm/download)

Dumping code:

    mongodump --gzip --archive > maincontent_mg.gz.dump

Restore example:

    mongorestore --gzip --archive=maincontent_pg.dump

